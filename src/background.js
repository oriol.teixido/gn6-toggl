String.prototype.toHHMMSS = function () {
  var sec_num = parseInt(this, 10);
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);

  if (hours   < 10) {hours   = '0' + hours;}
  if (minutes < 10) {minutes = '0' + minutes;}
  if (seconds < 10) {seconds = '0' + seconds;}
  return `${hours}:${minutes}:${seconds}`;
}

String.prototype.toHH = function () {
  var sec_num = parseInt(this, 10);
  return String(Math.floor(sec_num / 3600));
}

String.prototype.toMM = function () {
  var sec_num = parseInt(this, 10);
  return String(Math.floor((sec_num - (Math.floor(sec_num / 3600) * 3600)) / 60));
}

var GN6 = {
  togglApiUrl: 'https://api.track.toggl.com/api/v8/',
  togglApiToken: null,
  idRegex: null,
  maxHours: null,
  startDate: null,

  togglParseEntries: function (entries) {
    var elements = [];

    entries.forEach(entry => {
      let element = {};
      if ('description' in entry) {
        element.togglId = entry.id;
        element.description = entry.description;

        // Element Id
        let matches = entry.description.match(GN6.idRegex);
        if (matches) {
          element.id = matches[1];
        }

        // Date
        element.date = new Date(entry.start).toLocaleString('ca', { day: '2-digit', month: '2-digit', year: 'numeric' });

        // Duration
        if (entry.duration > 0) {
          element.hours = String(entry.duration).toHH();
          element.minutes = String(entry.duration).toMM();
          element.time = String(entry.duration).toHHMMSS();
        }

        element.validated = true;
        element.validated = element.validated && matches;
        element.validated = element.validated && entry.duration > 0 && entry.duration < GN6.maxHours * 3600;

        elements.push(element);
      }
    });
    return elements;
  },

  togglGetEntries: function() {   
    let start = new Date(GN6.startDate).toISOString();
    let end = new Date().toISOString();
    console.debug('GN6.togglGetEntries');
    return GN6.togglAjax('GET', `time_entries?start_date=${start}&end_date=${end}`, function(response) { 
      return GN6.togglParseEntries(JSON.parse(response));
    });
  },

  togglDeleteEntry: function(id) {
    console.debug('GN6.togglDeleteEntry');
    return GN6.togglAjax('DELETE', `time_entries/${id}`);
  },

  togglAjax: function(method, url, callback = function(response) { return response; }) {
    console.debug(`gn6.togglAjax(${url})`);
    
    if (!this.togglApiToken) {
      return new Promise((resolve, reject) => { 
        console.warn('Empty togglApiToken');        
        resolve('[]'); 
      });
    }

    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      let absoluteUrl = this.togglApiUrl + url;

      xhr.open(method, absoluteUrl); 
      xhr.setRequestHeader('Authorization', 'Basic ' +  btoa(this.togglApiToken + ':api_token'));
      xhr.addEventListener('load', function() { 
        if (this.status == 200) {
          resolve(callback(this.responseText));
        }
        else {
          reject(new Error(this.statusText + ' (' + this.status + ')'));
        }
      });
      xhr.addEventListener('error', function() { 
        reject(new Error('Network error on XMLHttpRequest (' + absoluteUrl + ')'));
      });
      xhr.send();
    });
  },

  checkSettings: function() {
    return GN6.togglApiToken && GN6.idRegex && !isNaN(GN6.maxHours) && !isNaN(GN6.startDate);
  },

  loadSettings: function() {
    console.debug('GN6.loadSettings');
    let getting = browser.storage.sync.get('settings');
    getting.then(function(result) {
      GN6.togglApiToken = result.settings.togglApiToken;
      try {
        GN6.idRegex = new RegExp(result.settings.idRegex);
      } catch(e) {
        GN6.idRegex = null;
      }
      GN6.maxHours = parseInt(result.settings.maxHours);
      GN6.startDate = Date.parse(result.settings.startDate);
      console.debug(`GN6.loadSettings - idRegex: ${GN6.idRegex}`);
      console.debug(`GN6.loadSettings - maxHours: ${GN6.maxHours}`);
      console.debug(`GN6.loadSettings - startDate: ${GN6.startDate}`);
      console.debug(`GN6.loadSettings - togglApiToken: ${GN6.togglApiToken}`);
    }, function(error) {
      console.error(`GN6.loadSettings: ${error}`);
    }); 
  },

  handleMessage: function(request, sender, sendResponse) {  
    console.debug('GN6.handleMessage');

    if (request.function == "loadSettings") {
      console.debug(`GN6.handleMessage - loadSettings()`);
      return GN6.loadSettings();
    }

    if (!GN6.checkSettings()) {
      return new Promise((resolve, reject) => {
        const url = browser.runtime.getURL('settings/settings.html').toString();
        reject(new Error('Cal configurar els paràmetres de l\'extensió. <a href="' + url + '">[Settings]</a>'));
      });
    }

    switch(request.function) {
      case 'togglGetEntries':
        console.debug(`GN6.handleMessage - togglGetEntries()`);
        return GN6.togglGetEntries();
      case 'togglDeleteEntry':
        console.debug(`GN6.handleMessage - togglDeleteEntry(${request.id}})`);
        return GN6.togglDeleteEntry(request.id);
    }
    console.error(`GN6.handleMessage: Function not found ${request.function}`);
    return null;
  }
};

GN6.loadSettings();
browser.runtime.onMessage.addListener(GN6.handleMessage);