function saveSettings(e) {
  e.preventDefault();
  let settings = {
    idRegex: document.querySelector('#idRegex').value,
    maxHours: document.querySelector('#maxHours').value,
    startDate: document.querySelector('#startDate').value,
    togglApiToken: document.querySelector('#togglApiToken').value
  };
  browser.storage.sync.set({ settings: settings }).then(function() {
    browser.runtime.sendMessage({
      function: "loadSettings"
    });
  });
}
  
function restoreSettings() {
  browser.storage.sync.get('settings').then(function(result) {
    document.querySelector('#idRegex').value = result.settings.idRegex || '';
    document.querySelector('#maxHours').value = result.settings.maxHours || '';
    document.querySelector('#startDate').value = result.settings.startDate || '';
    document.querySelector('#togglApiToken').value = result.settings.togglApiToken || '';
  }, 
  function(error) {
    console.error(error);
  });
}
  
document.addEventListener('DOMContentLoaded', restoreSettings);
document.querySelector('form').addEventListener('submit', saveSettings);
  