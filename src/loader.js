node = document.createElement('link');
node.type = 'text/css';
node.href = browser.runtime.getURL('css/main.css');
node.rel  = 'stylesheet';
node.media = 'all';
document.getElementsByTagName('head')[0].appendChild(node);

node.onload = function() {
  function loadHtml(url, selector, values = {}, callback = function(data) {}) {
    $.get(browser.runtime.getURL(url)).done(function(data) {
      var data = Mustache.render(data, values);
      callback($(selector).prepend($(data)));
    }).fail(function(jqXHR) {
      throw new HttpRequestException(jqXHR);
    });
  }

  function sendMessage(options, callback = function(response) {}) {
    browser.runtime.sendMessage(options).then(function(response) { 
      callback(response);
    }, function(e) {
      console.error(e.message);
      loadHtml('html/error.html', '.screenlet-body .caixacerca', { message: e.message });
    });
  }

  function togglGetEntries() {
    sendMessage({
      function: 'togglGetEntries', 
    }, function(response) { 
      listEntries(response);
    });
  }

  function togglDeleteEntry(id) {
    sendMessage({
      function: 'togglDeleteEntry',
      id: id
    });
  }

  function listEntries(entries) {
    if (entries.length > 0) {
      loadHtml('html/list.html', '.screenlet-body .caixacerca', { elements: entries }, function(data) {
        $(data).find("#gn6-toggl-entries-img").on("click", function() {
          $(this).toggleClass("gn6-toggl-rotated");
          $(this).closest(".caixacerc").find("table").toggle();
        });

        $("table#gn6-toggl-entries tr a").on("click", function() {
          let $tr = $(this).closest("tr");
          let id = $tr.data("id");
          let togglId = $tr.data("toggl-id");
          let date = $tr.data("date");
          let hours = $tr.data("hours");
          let minutes = $tr.data("minutes");
          gn6TimeAssign(id, date, hours, minutes, function() {
            togglDeleteEntry(togglId);
            // TODO: Check si s'ha esborrat correctament!
            $tr.remove();
          });
        });
      });
    }
  }

  function gn6TimeAssign(id, date, hours, minutes, callback) {
    console.debug(`gn6TimeAssign(${id},${date},${hours},${minutes})`);

    $.get("https://gn6.upc.edu/tiquets/control/tiquetDetallAssignacioHistoria?requirementId=" + id).done(function(data) {
      let form = $(data).find('#afegirImputacio').serializeArray();
      form.push({'name': 'dataImputacio', 'value': date.replace(/\//g, "-")});
      form.push({'name': 'horesImputadesHelper', 'value':  hours.toString()});
      form.push({'name': 'minutsImputatsHelper', 'value': minutes.toString()});
      form.push({'name': 'horesImputades', 'value': "0"});
      form.push({'name': 'minutsImputats', 'value': (hours * 60 + minutes).toString()});
  
      let post = {};
      for (var i = 0; i < form.length; i++){
          post[form[i]['name']] = form[i]['value'];
      }

      $.post("https://gn6.upc.edu/tiquets/control/imputarTempsTasca", $.param(post)).done(function(data) {
          let found = false;
          $(data).find(".screenlet-body tr.TRbackground, .screenlet-body tr.TRbackground_b").each(function () {
            let data = $(this).find("td:nth-child(1) span").text();
            let tecnic = $(this).find("td:nth-child(2) span").text();
            let temps = $(this).find("td:nth-child(3) span").text();
            if (data.includes(date.replace(/\//g, "-")) && tecnic.includes(post['personaDecorator']) && temps.includes(hours.toString() + " Hores " + minutes.toString() + " Minuts")) {
              found = true;
            }                
          });
          if (found) {
            callback();
          }
          else {
            let message = `Time not found (${id},${date},${hours},${minutes},${post['personaDecorator']})`;
            console.error(message);
            loadHtml('html/error.html', '.screenlet-body .caixacerca', { message: message });
          }
      }).fail(function(e) {
        console.error(e.message);
        loadHtml('html/error.html', '.screenlet-body .caixacerca', { message: e.message });
      });
    }).fail(function(e) {
      console.error(e.message);
      loadHtml('html/error.html', '.screenlet-body .caixacerca', { message: e.message });
    });
  }

  togglGetEntries();
};

