# GN6 - Toggl

# Development

# Requisites

```bash
sudo apt-get install npm docker.io
sudo npm install --global web-ext
```

## Execute

```bash
cd src
web-ext run
```

## Build

# Test

```bash
docker build -t gn6-toggl .
```

## Build

```bash
docker run -u $USER -v $PWD:/app -it gn6-toggl web-ext build --source-dir ./src --overwrite-dest
```
